package Pentalog.GeometricShapes;

public interface GeometricShape {

	void SetDimensions();

	double getArea();

	double getPerimeter();
}
