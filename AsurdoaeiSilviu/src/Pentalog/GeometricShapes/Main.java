package Pentalog.GeometricShapes;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		while (true)

			ChoseYourShape(scanner);

	}

	public static void ChoseYourShape(Scanner scanner) {
		try {

			// scanner = new Scanner(System.in);
			System.out.println("Chose 1 for Circle, 2 for Rectangle and 3 for Triangle:");
			int Shape = scanner.nextInt();
			double area = 0, perimeter = 0;
			DecimalFormat numberFormat = new DecimalFormat("#.00");

			switch (Shape) {

			case 1:
				Circle cr = new Circle(scanner);
				cr.SetDimensions();
				area = cr.getArea();
				System.out.println("Area of Circle is: " + numberFormat.format(area) + " cm!");
				perimeter = cr.getPerimeter();
				System.out.println("Perimeter of Circle is: " + numberFormat.format(perimeter) + " cm!");
				break;
			case 2:
				Rectangle rc = new Rectangle(scanner);
				rc.SetDimensions();
				area = rc.getArea();
				System.out.println("Area of Rectangle is: " + numberFormat.format(area) + " cm!");
				perimeter = rc.getPerimeter();
				System.out.println("Perimeter of Circle is: " + numberFormat.format(perimeter) + " cm!");
				break;

			case 3:
				Triangle tr = new Triangle(scanner);
				tr.SetDimensions();
				area = tr.getArea();
				System.out.println("Area of Triangle is: " + numberFormat.format(area) + " cm!");
				perimeter = tr.getPerimeter();
				System.out.println("Perimeter of Triangle is: " + numberFormat.format(perimeter) + " cm!");
				System.out.println(" ");
				break;
			default:
				System.out.println("Please chose a number from 1 to 3!");
				break;
			}

		} catch (Exception e) {
			System.out.println("Only numbers are valid!");
			scanner.next();
		}

	}
}
