package Pentalog.GeometricShapes;

import java.util.Scanner;

public class Triangle implements GeometricShape {

	private Scanner scanner;
	private static double base;
	private static double leftside;
	private static double rightside;

	public Triangle(Scanner scanner) {

		base = 0.00;
		leftside = 0.00;
		rightside = 0.00;
		this.scanner = scanner;
	}

	public double getLeftside() {
		return leftside;
	}

	public double getRightside() {
		return rightside;
	}

	public Scanner getScanner() {
		return scanner;
	}

	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

	public double getBase() {
		return base;
	}

	public void SetDimensions() {

		getSides();

		while (base + rightside <= leftside || leftside + rightside <= base || base + leftside <= rightside) {
			System.out.println("Sides entered cannot form a triangle!");
			getSides();
		}
	}

	public void getSides() {

		System.out.println("Enter the base of the Triangle:\n");

		base = scanner.nextDouble();
		while (base <= 0) {
			System.out.println(" The base should be a pozitive number > 0");
			base = scanner.nextDouble();
		}

		System.out.println("Enter the left side of the Triangle:");

		leftside = scanner.nextDouble();
		while (leftside <= 0) {
			System.out.println(" The leftside should be a pozitive number > 0");
			leftside = scanner.nextDouble();
		}

		System.out.println("Enter the right side of the Triangle:");

		rightside = scanner.nextDouble();
		while (rightside <= 0) {
			System.out.println(" The rightside should be a pozitive number > 0");
			rightside = scanner.nextDouble();
		}

	}

	public double getArea() {

		double area = 0, area2 = 0;
		area = (base + leftside + rightside) / 2;
		area2 = Math.sqrt(area * (area - base) * (area - leftside) * (area - rightside));
		return area2;
	}

	public double getPerimeter() {

		return base + leftside + rightside;

	}
}
