package Pentalog.GeometricShapes;

import java.util.Scanner;

public class Circle implements GeometricShape {

	private Scanner scanner;
	private static double radius;

	public Circle(Scanner scanner) {
		this.scanner = scanner;
		radius = 0.00;
	}

	public Scanner getScanner() {
		return scanner;
	}

	public double getRadius() {
		return radius;
	}

	public void SetDimensions() {
		System.out.println("Enter the radius of the Circle: ");

		radius = scanner.nextDouble();

		while (radius <= 0) {

			System.out.println("You need to insert a radius bigger then 0!");
			radius = scanner.nextDouble();
		}
	}

	public double getArea() {

		return radius * radius * Math.PI;

	}

	public double getPerimeter() {

		return 2 * Math.PI * radius;

	}
}
