package Pentalog.GeometricShapes;

import java.util.Scanner;

public class Rectangle implements GeometricShape {

	private Scanner scanner;
	private static double length;
	private static double width;

	public Rectangle(Scanner scanner) {

		length = 0.0;
		width = 0.0;
		this.scanner = scanner;

	}

	public static double getLength() {
		return length;
	}

	public static void setLength(double length) {
		Rectangle.length = length;
	}

	public static double getWidth() {
		return width;
	}

	public static void setWidth(double width) {
		Rectangle.width = width;
	}

	public void SetDimensions() {
		System.out.println("Enter the length of the Rectangle: ");
		length = scanner.nextDouble();
		while (length <= 0) {
			System.out.println(" The length should be a pozitive number > 0");
			length = scanner.nextDouble();
		}

		System.out.println("Enter the width side of the Triangle:");
		System.out.println(" ");
		width = scanner.nextDouble();
		while (width <= 0) {
			System.out.println(" The width should be a pozitive number > 0");
			width = scanner.nextDouble();
		}
	}

	public double getArea() {

		return width * length;

	}

	public double getPerimeter() {

		return 2 * (width + length);

	}

}
