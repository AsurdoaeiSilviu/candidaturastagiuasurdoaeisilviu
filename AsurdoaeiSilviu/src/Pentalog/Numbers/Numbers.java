package Pentalog.Numbers;

import java.util.ArrayList;

public class Numbers {

	public static void main(String[] args) {

		int numbers[] = new int[] { 234, 678, 131, 101, 199, 346 };
		ArrayList<Integer> palindromes = new ArrayList<Integer>();
		ArrayList<Integer> primes = new ArrayList<Integer>();

		int min = numbers[0];
		int max = numbers[0];

		for (Integer i : numbers) {

			if (i < min)
				min = i;
			if (i > max)
				max = i;
			if (IsPalindrome(i))
				palindromes.add(i);
			if (isPrime(i))
				primes.add(i);

		}

		System.out.println("Largest number: " + max);
		System.out.println("Smallest number: " + min);
		System.out.println("Palindromes: " + palindromes.toString());
		System.out.println("Prime numbers: " + primes.toString());

	}

	public static boolean isPrime(int number) {
		// check if n is a multiple of 2
		if (number % 2 == 0)
			return false;
		// if not, then just check the odds
		for (int i = 3; i * i <= number; i += 2) {
			if (number % i == 0)
				return false;
		}
		return true;
	}

	public static boolean IsPalindrome(int number) {
		int aux = number;
		int reversedNumber = 0;
		int temp = 0;

		while (aux > 0) {
			temp = aux % 10;
			aux = aux / 10;
			reversedNumber = reversedNumber * 10 + temp;
		}

		if (number == reversedNumber)
			return true;
		return false;
	}
}
